var numbers;
var arr = [];
var resultString = "";

function generate() {
    numbers = document.getElementById("input").value;
    resultString = "";
    arr = [];
    for (var i = 0; i <= numbers; i++) {
        arr.push(i);
    }
    document.getElementById("number").innerHTML = "Last number: "
    out();
}

function pull() {
    if (arr.length - 1) {
        resultString = "";
        number = randomInteger(1, arr.length - 1);
        var tempNumber = arr[number];
        arr.splice(number, 1);
        document.getElementById("number").innerHTML = "Last number: " + tempNumber;
        out();
    } else {
        document.getElementById("number").innerHTML = "Empty"
    }
}

function out(){
    for (var i = 1; i <= arr.length - 1; i++) {
        resultString = resultString + " " + arr[i];
    }
    document.getElementById("list").innerHTML = resultString;
}

function randomInteger(min, max) {
    var rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
}